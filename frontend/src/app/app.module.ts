import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import { TasksModule } from "./tasks/tasks.module";
import { TaskService } from "./services/task.service";

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports : [
        BrowserModule,
        TasksModule,
        FormsModule
    ],
    providers: [TaskService],
    bootstrap: [AppComponent]
})

export class AppModule {}