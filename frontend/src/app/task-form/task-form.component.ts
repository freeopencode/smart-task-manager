import { Component } from '@angular/core';
import { FormsModule, NgForm } from '@angular/forms';
import { TaskService } from '../services/task.service';
import { Task } from '../models/task.model';

@Component({
  selector: 'app-task-form',
  standalone: true,
  imports: [FormsModule],
  templateUrl: './task-form.component.html',
  styleUrl: './task-form.component.css',
})
export class TaskFormComponent {
  task: Task = {
    title: '',
    description: '',
    priority: '',
  };

  constructor(private taskService: TaskService) {}

  addTask(form: NgForm): void {
    if (form.valid) {
      this.taskService.addTask(this.task);
      console.log('Tâche ajoutée : ', this.task);
      this.task = {
        title: '',
        description: '',
        priority: '',
      };
      form.resetForm();
    }
   
  }
}
