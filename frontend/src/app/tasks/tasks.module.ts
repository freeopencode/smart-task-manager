import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from '@angular/forms';
import { TaskFormComponent } from "../task-form/task-form.component";
import { TaskListComponent } from "../task-list/task-list.component";
import { TaskMatrixComponent } from "../task-matrix/task-matrix.component";
import { TaskService } from "../services/task.service";

@NgModule({
    declarations: [
        TaskFormComponent,
        TaskListComponent,
        TaskMatrixComponent
    ],
    imports : [
        CommonModule,
        FormsModule
    ],
    exports: [
        TaskFormComponent,
        TaskListComponent,
        TaskMatrixComponent
    ],
    providers: [TaskService]
})

export class TasksModule {}