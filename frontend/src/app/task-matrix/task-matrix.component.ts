import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskService } from '../services/task.service';
import { Task } from '../models/task.model';

@Component({
  selector: 'app-task-matrix',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './task-matrix.component.html',
  styleUrl: './task-matrix.component.css'
})
export class TaskMatrixComponent implements OnInit {
  tasks: Task[] = [];

  constructor(private taskService: TaskService) {}

  ngOnInit(): void {
    this.taskService.tasks$.subscribe(tasks => {
      this.tasks = tasks;
    })
  }

  getTasksByPriority(priority: string): Task[] {
    return this.tasks.filter(task => task.priority === priority);
  }

  deleteTask(task: Task): void {
    this.taskService.deleteTask(task);
  }
}
