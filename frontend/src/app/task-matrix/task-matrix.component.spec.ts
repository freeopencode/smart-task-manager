import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatrixGridComponent } from './task-matrix.component';

describe('MatrixGridComponent', () => {
  let component: MatrixGridComponent;
  let fixture: ComponentFixture<MatrixGridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatrixGridComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MatrixGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
