import { Injectable } from "@angular/core";
import { Task } from "../models/task.model";
import { BehaviorSubject } from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class TaskService {
    private tasks: Task[] = [];
    private tasksSubject = new BehaviorSubject<Task[]>(this.tasks);

    tasks$ = this.tasksSubject.asObservable();

    constructor() {}

    // Pour afficher la liste des tâches
     getTasks(): Task[] {
        return this.tasks;
    }

    // Pour ajouter une tâche
    addTask(task: Task): void {
        this.tasks.push(task);
        this.tasksSubject.next(this.tasks);
        console.log('Tâche ajoutée au service :', task);
    }

    // Pour effacer une tâche
    deleteTask(task: Task): void {
        const index = this.tasks.indexOf(task);
        if (index > -1) {
            this.tasks.splice(index, 1);
            this.tasksSubject.next(this.tasks);
        }
    }

   
}